import request from '@/utils/request'

export function getServerInfo(data) {
  return request({
    url: '/bpm/monitor/getServerInfo',
    method: 'post',
    data
  })
}


import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/bpm/user/login',
    method: 'post',
    data
  })
}

export function getInfo(data) {
  return request({
    url: '/bpm/user/getUserInfo/' + data,
    method: 'post'

  })
}

export function logout() {
  return request({
    url: '/bpm/user/logout',
    method: 'post'
  })
}

export function getOrganUserTree(data) {
  return request({
    url: '/bpm/user/getOrganUserTree',
    method: 'post',
    data
  })
}

export function getUserList(data) {
  return request({
    url: '/bpm/user/getUserList',
    method: 'post',
    data
  })
}

export function deleteUser(data) {
  return request({
    url: '/bpm/user/delete',
    method: 'post',
    data
  })
}

export function updateUser(data) {
  return request({
    url: '/bpm/user/update',
    method: 'post',
    data
  })
}

export function insertUser(data) {
  return request({
    url: '/bpm/user/insert',
    method: 'post',
    data
  })
}

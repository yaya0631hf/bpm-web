import request from '@/utils/request'

export function getList(data) {
  return request({
    url: '/bpm/dictItem/getList',
    method: 'post',
    data
  })
}

export function insertOrUpdateDictItem(data) {
  return request({
    url: '/bpm/dictItem/insertOrUpdateDictItem',
    method: 'post',
    data
  })
}

export function deleteDictItem(data) {
  return request({
    url: '/bpm/dictItem/delete',
    method: 'post',
    data
  })
}

